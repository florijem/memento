# Installation de Void Linux #

## Premiers Pas ##

Demarrer Void Linux sur un support amovible.

Installer `gptfdisk` :

```
$ qnon
$ voidlinux
$ sudo su
# loqdkeys fr)lqtin9
# xbps-install gptfdisk
```

Chercher le nom du disque dur que l'on va utiliser pour l'installation :

```
# lsblk
```

Créer une nouvelle table de partition sur le disque dur.  
Remplacer `X` avec la lettre de notre partition trouvée ci-avant :

```
# gdisk /dev/sdX
> x
> z
>> Y
>> Y
# fdisk /dev/sdX
> g
> w
```

Écrire nos partitions avec l'outil `cgdisk` :

```
# cgdisk /dev/sdx
```

> Pour chaque partition créer, nous serons interrogés concernant une position de départ, une taille et un type.  
> La première partition doit être de type `ef00`, la `swap` de type `8200` et le reste de type `8300`.  
> Si après, un exemple montrant des partitions separées pour les points de montage `/boot`, `/boot/efi`, `/`, `/home`, `/var` e `/tmp`.
>
> ```
> Part.     #     Size        Partition Type            Partition Name
> --------------------------------------------------------------------
> 1               512.0 MiB   EFI System                EFI
> 2               1024.0 MiB  Linux filesystem          GRUB
> 3               4.0 GiB     Linux swap                swap
> 4               2.0 GiB     Linux filesystem          tmp
> 5               6.0 GiB     Linux filesystem          var
> 6               22.0 GiB    Linux filesystem          root
> 7               454.2 GiB   Linux filesystem          home
> ```
>
> Après avoir paramétré nos partition, nouq sélectionnons `Write`, ensuite `Verify`, pour vérifier s'il y a eu des erreurs, et finalement `Quit`.

Vérifier la réussite de la création de nos partitions :

```
# lsblk
```

> La liste devrait ressembler à celà :
>
> ```
> NAME       MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINT
> sda          8:0    0 238.5G  0 disk
> sda1         8:1    0   512M  0 part
> sda2         8:2    0  1024M  0 part
> sda3         8:3    0     4G  0 part
> sda4         8:4    0     2G  0 part
> sda5         8:5    0     6G  0 part
> sda6         8:6    0    22G  0 part
> sda7         8:7    0 454.2G  0 part
> ```

Créer les systèmes de fichiers appropriés pour les partitions nouvellement créées :

```
# mkfs.vfat -F32 /dev/sda1
# mkfs.ext4 /dev/sda2
# mkfs.ext4 /dev/sda4
# mkfs.ext4 /dev/sda5
# mkfs.ext4 /dev/sda6
# mkfs.ext4 /dev/sda7
```

Activer notre partition `swap :

```
# mkswap /dev/sda3
```

Monter les volumes, en faisant si nécessaire la création des dossiers pour les points de montage (l'ordre est important) :

```
# mount /dev/sda6 /mnt
# mkdir /mnt/boot
# mkdir /mnt/tmp
# mkdir /mnt/var
# mount /dev/sda2 /mnt/boot
# mkdir /mnt/boot/efi 
# mount /dev/sda1 /mnt/boot/efi
# mount /dev/sda4 /mnt/tmp
# mount /dev/sda5 /mnt/var
```

## Installer Void Linux ##

Si vous travaillez pour une installation avec une architecture différente de celle sur laquelle vous êtes, accorder la variable `XBPS_ARCH` et l'exporter aux suites que `xbps-install` trouve les fichiers pour l'architecture désirée.

> Exemple :
>
> ```
> export XBPS_ARCH=x86_64
> export XBPS_ARCH=x86_64-musl
> ```

Installer Void Linux et `GRUB` sur le système de fichiers monté :

```
# export XBPS_ARCH=x86_64 && xbps-install -S -R https://repo.voidlinux.eu/current -r /mnt base-system grub-x86_64-efi</pre>
# export XBPS_ARCH=x86_64-musl && xbps-install -S -R https://repo.voidlinux.eu/current/musl -r /mnt base-system grub-x86_64-efi
```

Après l'installation, paramétrer notre prison `chroot`, et cloisonner dans notre système de fichiers montés :

```
# mount -t proc proc /mnt/proc
# mount -t sysfs sys /mnt/sys
# mount -o bind /dev /mnt/dev
# mount -t devpts pts /mnt/dev/pts
# cp -L /etc/resolv.conf /mnt/etc/
# cd /mnt
# chroot /mnt
```

Vérifier la structure des fichiers pour confirmer la bonne installation :

```
# ls -la
```

> La liste devrait ressemblait à celle qui suit :
>
> ```
> Total 12  
> drwxr-xr-x 16 root root 4096 Jan 17 15:27 .  
> drwxr-xr-x  3 root root 4096 Jan 17 15:16 ..  
> lrwxrwxrwx  1 root root    7 Jan 17 15:26 bin -> usr/bin  
> drwxr-xr-x  4 root root  127 Jan 17 15:37 boot  
> drwxr-xr-x  2 root root   17 Jan 17 15:26 dev  
> drwxr-xr-x 26 root root 4096 Jan 17 15:27 etc  
> drwxr-xr-x  2 root root    6 Jan 17 15:26 home  
> lrwxrwxrwx  1 root root    7 Jan 17 15:26 lib -> usr/lib  
> lrwxrwxrwx  1 root root    9 Jan 17 15:26 lib32 -> usr/lib32  
> lrwxrwxrwx  1 root root    7 Jan 17 15:26 lib64 -> usr/lib  
> drwxr-xr-x  2 root root    6 Jan 17 15:26 media  
> drwxr-xr-x  2 root root    6 Jan 17 15:26 mnt  
> drwxr-xr-x  2 root root    6 Jan 17 15:26 opt  
> drwxr-xr-x  2 root root    6 Jan 17 15:26 proc  
> drwxr-x---  2 root root   26 Jan 17 15:39 root  
> drwxr-xr-x  3 root root   17 Jan 17 15:26 run  
> lrwxrwxrwx  1 root root    8 Jan 17 15:26 sbin -> usr/sbin  
> drwxr-xr-x  2 root root    6 Jan 17 15:26 sys  
> drwxrwxrwt  2 root root    6 Jan 17 15:15 tmp  
> drwxr-xr-x 11 root root  123 Jan 17 15:26 usr  
> drwxr-xr-x 11 root root  150 Jan 17 15:26 var
> ```

Créer le mot de passe de l'utilisateur `root` et paramétrer ses permissions :

```
# passwd root
# chown root:root /
# chmod 755 /
```

Donner un nom à l'ordinateur :

```
# echo nom_de_l_ordinateur /etc/hostname
```

Éditer le fichier de configuration `rc.conf` :

```
# vi /etc/rc.conf
```

> ```
> HOSTNAME="nom_de_l_ordinateur";
>
> # Set RTC to UTC or localtime.
> HARDWARECLOCK="UTC"
>
> # Set timezone, availables timezones at /usr/share/zoneinfo.
> TIMEZONE="Europe/Paris"
>
> # Keymap to load, see loadkeys(8).
> KEYMAP="fr-latin9"
>
> # Console font to load, see setfont(8).
> #FONT="lat9w-16"
> ```

Éditer notre fichier `fstab` avec les identifiants de chacune des partitions :

```
# blkid | grep sdX >> /etc/fstab
# vi /etc/fstab
```

> Formater le fichier `fstab` pour qu'il ressemble à celui si après.  
> Si le disque dur est de type SSD, ajouter les options `noatime,nodiratime,` après `rw,`.
>
> ```
> #
> # See fstab(5).
> #
> # <file system> <dir>   <type>  <options>               <dump>  <pass>
> #tmpfs           /tmp    tmpfs   defaults,nosuid,nodev   0       0t
> UUID=dcdd0c5a-020b-4167-a10d-cb81d71e2ae6 /         ext4 rw,noatime,nodiratime,discard              0 1
> UUID=d7d2ddae-cb94-4aea-bc4f-4784d6b3cc8e /boot     ext4 rw,noatime,nodiratime,discard              0 2
> UUID=C071-6887                            /boot/efi vfat rw,noatime,nodiratime,discard              0 2
> UUID=97060d6a-039e-469f-b0aa-2fca2e33f464 /tmp      ext4 rw,noatime,nodiratime,discard,nosuid,nodev 0 2
> UUID=4fb9395f-42c2-4b27-8545-1e9c1703c94d /var      ext4 rw,noatime,nodiratime,discard,nosuid,nodev 0 2
> UUID=4a24a9e9-3c04-4aa5-826b-da2122347094 /home     ext4 rw,noatime,nodiratime,discard              0 2
> UUID=cfb8be30-1866-44a6-bdf5-60ced2a454f4 swap      swap rw,noatime,nodiratime,discard              0 0
> ```

Paramétrer notre localisation en décommentant les lignes suivantes du fichier `/etc/default/libc-locales` :

```
# vi /etc/default/libc-locales
```

> ```
> en_US.UTF-8 UTF-8  
> fr_FR.UTF-8 UTF-8
> ```

```
# echo "LANG=fr_FR.UTF-8\nLANGUAGE=fr_FR:en_US:en\nLC_COLLATE=C" > /etc/locale.conf
```

Reconfigurer le système avec les localisations choisies :

```
# xbps-reconfigure -f glibc-locales
```

Éditer le fichier `dracut.conf`, activer `hostonly` et ajouter le support pour notre tampon `/tmp` :

```
# echo "hostonly=\"yes\"\ntmpdir=/tmp" >> /etc/dracut.conf
```

Trouver la version du noyau de linux installée :

```
# cd /lib/modules
# ls -la
```

> La liste doit ressembler à celle suivante :
>
> ```
> drwxr-xr-x  3 root root   21 Jan 31 15:22 .  
> drwxr-xr-x 23 root root 8192 Jan 31 15:22 ..  
> drwxr-xr-x  3 root root 4096 Jan 31 15:22 4.17.6_2
> ```

Mettre à jour `dracut` :

```
# dracut --force --hostonly '' 4.17.6_2
```

## touche Finale ##

Nous sommes maintenant prêt à installer `GRUB` et configurer notre installation :

```
# grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=n6_d_lordinatcr --boot-directory=/boot --recheck --debug
# xbps-reconfigure -f linux4.17
```

De plus, pour éviter tout problème de démarrage par la suite, spécialement dans VirtualBox, il est conseillé de rajouter les commandes suivantes :

```
# mkdir /boot/efi/EFI/boot
# cp /boot/efi/EFI/nom_de_l_ordinateur/grubx64.efi /boot/efi/EFI/boot/bootx64.efi
```

Après le succès de l'installation et la configuration, nous pouvons sortir de la prison, de `sudo`, démonter notre système de fichiers, et redémarrer sur notre nouvelle installation de Void Linux !

```
# exit
# exit
$ sudo umount -R /mnt
$ sudo reboot
```

## Personnalisation ##

Mettre à jour Void :

```
# xbps-install -Suv
```

Activer le réseau avec `dhcpcd` et désactiver `IPv6` :

```
# sed -i "/::1/ s/^/#/" /etc/hosts
# sed -i "s/slaac private/slaac hwaddr
# echo "noipv6rs\noipv6" >> /etc/dhcpcd.conf
# ln -s /etc/sv/dhcpcd /var/service/dhcpcd
```

Ajouter un nouvel utilisateur (remplacer `mon_nom` par celui désiré) :

```
# useradd -m -s /bin/bash -U -G wheel,users,audio,disk,video,cdrom,optical,storage,input -c 'Mon Nom complet' mon_nom
```

Définir un mot de passe pour l'utilisateur `mon_nom` créé :

```
# passwd mon_nom
```

Autoriser le nouvel utilisateur a accéder au système via `sudo` :

```
# visudo
```

> Décommenter la ligne suivante :
>
> ```
> # %wheel ALL=(ALL) ALL
> %wheel ALL=(ALL) ALL
> ```
>
> Ajouter la ligne suivante au dessus de la précédente pour configurer `sudo` afin qu'il demande le mot de passe de `root` au lieu du mot de passe de l'utilisateur faisant partie du groupe `wheel` obligatoirement :
>
> ```
> Defaults:%wheel rootpw
> %wheel ALL=(ALL) ALL
> ```
>
> Chercher la ligne suivante pour ajouter des groupes de commandes à sa suite :
>
> ```
> # Cmnd_Alias
> ```
>
> Ajouter les groupes de commandes pour la gestion de l'alimentation et de la mise à jour :
>
> ```
> Cmnd_Alias PUISSANCE = /usr/bin/halt, /usr/bin/poweroff, /usr/bin/reboot, \
>                        /usr/bin/shutdown, /usr/bin/zzz, /usr/bin/ZZZ
> Cmnd_Alias MISEAJOUR = /usr/bin/xbps-install -Suv
> ```
>
> Chercher la ligne suivante pour ajouter des permissions à sa suite :
>
> ```
> # %wheel ALL=(ALL) NOPASSWD: ALL
> ```
>
> Ajqte le permisy6 dekzekute la kom4d `sudo kom4d` pqr1 nutilizatcr du grqp `wheel` s4 dcm4de d mo d pas :
> Ajouter les permissions d'exécuter la commande `sudo commande` pour un utilisateur du groupe `wheel` sans demander de mot de passe :
>
> ```
> %wheel ALL = (root) NOPASSWD: PUISSANCE
> %wheel ALL = (root) NOPASSWD: MISEAJOUR
> ```
>
> 1si, 1 nutilizatcr du grqp `wheel` pqra ekzekute le kom4d `sudo kom4d` s4 zokun dcm4d d mo d pas.
> Ainsi, un utilisateur du groupe `wheel` pourra exécuter les commandes `sudo commande` sans aucune demande de mot de passe.

Installer une base logiciel :

```
$ sudo xbps-install -S xtools alsa-utils iptables openntpd rsync wget curl gnupg2 pass htop \
                       nnn ex-vi git fossil ffmpeg links bzip2 cabextract cpio gzip libarchive \
                       lrzip lz4 lzip lzop p7zip tar unarj unzip xz zip cmark acpi
```

Activer le service `openntpd` pour la synchronisation de l'horloge au démarrage :

```
$ sudo ln -s /etc/sv/openntpd /var/service/
```

Configurer la gestion du son :

```
$ sudo ln -s /etc/sv/alsa /var/service/
$ sudo alsamixer
$ sudo alsactl store
```

Activer le service `acpid` pour l'observation de l'alimentation de l'ordinateur :

```
$ sudo ln -s /etc/sv/acpid /var/service/
```

Générer une paire de clés `gpg` avec un identifiant, ici nom, prénom et une adresse courriel pour une validité d'un an :

```
gpg2 --quick-gen-key 'nom Prénom <nom_prenom@domaine.com>' ed25519 cert 1y
gpg2 --quick-gen-key 4pr1t_afiche_apre_la kom4d_presed4t ed25519 sign 1y
gpg2 --quick-gen-key 4pr1t_afiche_apre_la kom4d_presed4t cv25519 encr 1y
```

Initialiser un dépôt de mot de passe avec le courriel déclaré précédemment :

```
pass init nom_prenom@domaine.com
```

Paramétrer `ssh` pour augmenter sa sécurité et l'activer au démarrage :

```
$ su root
# PORALEA=$(( 49152 + ( $( od -An -N2 -i /dev/random ) ) % (65535 - 49152 + 1 ) ))
# cat >> /etc/ssh/sshd_config << EOF
> Port $PORALEA
> PermitRootLogin no
> X11Forwarding no
> AllowUsers mon_nom
> AllowGroups mon_nom
> DenyUsers root
> DenyGroups root
> EOF
# echo $PORALEA
# exit
$ sudo ln -s /etc/sv/sshd /var/service/
```

> Le port `22` est celui qui est utiliser par défaut pour `ssh`. Ainsi, aux premiers abords, les robots ou pirate vont attaquer par se port. donc ici, on modifie ce port par un numéro aléatoire entre `49152` et `65535` (plage de numéro désallouée).
> Interdire l'accès aux superutilisateur `root` en `ssh`. Ainsi, on devra se connecter en tant que simple utilisateur avant d'obtenir les droits d'administrateur avec la commande `su`.
> 1terdir l rcle ver l servcr dafihaj `X`. 
> Interdire le relais vers le service d'affichage `X`.
> Seul les utilisateurs ou les membres des groupes listés seront autorisés à se connecter. Ajouter des utilisateurs ou groupes en les mettant à la suite des autres séparés par un espace.
> Le contenu affiché de la variable `PORALEA` est désormais le numéro de port attribué à `ssh`. Dans les étapes suivantes de ce guide remplacer `mon_port` par celui-ci. Il sera nécessaire à chaque connexion `ssh`, donc il faut le noter pour le retrouver en cas de besoin.

Installer le serveur d'impression `CUPS` et activer le service :

```
$ sudo xbps-install cups cups-filters hplip foomatic-db
$ sudo ln -s /etc/sv/cupsd /var/service/
$ sudo ln -s /etc/sv/cups-browsed /var/service/
```
> Accéder à l'interface web d'administration de `CUPS` avec l'URL http://localhost:631/admin

Installer le service graphique `Xorg` et des polices de caractères :

```
$ sudo xbps-install -S xorg-minimal xorg-fonts xorg-apps xinit xterm xf86-video-intel
$ sudo xbps-install -S dina-font gohufont artwiz-fonts profont font-tamsyn terminus-font \
                       tewi-font font-unifont-bdf font-bh-ttf fonts-croscore-ttf dejavu-fonts-ttf \
                       fonts-droid-ttf noto-fonts-ttf liberation-fonts-ttf ttf-ubuntu-font-family \
                       font-fira-otf freefont-ttf font-hack-ttf font-inconsolata-otf \
                       font-sourcecodepro
$ mkfontscale
$ mkfontdir
```

Configurer le clavier :

```
$ cd /etc/X11
$ sudo mkdir xorg.conf.d
$ cd xorg.conf.d
$ sudo vi 10-keyboard.conf
> Section "InputClass"
>        Identifier "klavye"
>        MatchIsKeyboard "on"
>        MatchDevicePath "/dev/input/event*"
>        Driver "libinput"
>        Option "XkbLayout" "fr"
>        Option "XkbModel" "pc102"
>        Option "XkbVariant" "oss_latin9"
> EndSection
```

Configurer le `touchpad` :

```
$ cd /etc/X11/xorg.conf.d
$ sudo vi 20-touchpad.conf
> Section "InputClass"
>        Identifier "sqri"
>        MatchIsTouchpad "on"
>        MatchDevicePath "/dev/input/event*"
>        Driver "libinput"
>        Option "Tapping" "on"
>        Option "TappingButtonMap" "lrm"
>        Option "TappingDrag" "on"
> EndSection
```

Installer `RedShift` pour adapter la luminosité de l'écran en fonction des heures de la journée et la position du soleil :

```
$ sudo xbps-install -S redshift
$ echo "pgrep redshift &> /dev/null || redshift &> /dev/null &" >> .xinitrc
$ mkdir -r .config/redshift
$ vi .config/redshift/redshift.conf
> [redshift]
> temp-day=5700
> temp-night=3500
> fade=1
> gamma=0.8
> location-provider=manual
> adjustment-method=randr
>
> [manual]
> ; trouver la latitude et la longitude de la ville désirée sur www.geonames.org, ici par exemple, Paris :
> lat=48.853
> lon=2.349
> 
> [randr]
> screen=0
```

Installer le gestionnaire de fenêtre `CWM` :

```
$ sudo xbps-install -S cwm dmenu passmenu
$ echo 'exec cwm' >> .xinitrc
```

Installer les logiciel pour gérer les images :

```
$ sudo xbps-install -S ImageMagick feh inkscape
```

Installer la suite bureautique `LibreOffice` et le lecteur de PDF `MuPDF` :

```
$ sudo xbps-install -S libreoffice-common libreoffice-base libreoffice-calc libreoffice-draw libreoffice-writer libreoffice-i18n-fr mupdf
```

Installer le naviguateur internet `Firefox` et le gestionnaire de téléchargement de torrent `Transmission` :

```
$ sudo xbps-install -S firefox firefox-i18n-fr transmission
$ sudo ln -s /etc/sv/transmission-daemon /var/service/
```

> Accéder à l'interface web d'administration de `Transmission` avec l'URL http://localhost:9091/transmission/web/

Installer le lecteur de flux `Newsboat` et le configurer avec un fichier `OPML` :

```
$ sudo xbps-install -S newsboat
$ newsboat
$ newsboat -i /hcm1/n6_d_fihye.opml
$ vi .newsboat/config
> show-read-articles no
> show-read-feeds no
```

Installer le lecteur `MOC` et le configurer :

```
$ sudo xbps-install -S moc
$ mkdir ~/muzik
$ mocp
> q
$ mocp -x
$ cp /usr/share/doc/moc/config.example ~/.moc/config
$ echo 'MusicDir = "/home/muajem/muzik"' >> ~/.moc/config
```

Installer le lecteur vidéo `MPV` :

```
$ sudo xbps-install -S mpv youtube-dl
```
